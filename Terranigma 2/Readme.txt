Terranigma 2 - The Contiunance:
Demo V.1.0


Anleitung:

1.Rechtliche Darstellung
2.Installation
3.Steuerung
4.FAQ
5.Begriffs-Erkl�rungen
6.Credits

---------------------------------------------------

1.Rechtliche Darstellung:

Dieses Spiel wurde ohne Genehmigung von Nintendo, Square Enix oder Quintet erstellt. S�mtliche Grafiken, Sounds, Charaktere, Gameplay-Features etc. aus "Terranigma" sind
rechtliches Eigentum der jeweiligen Firmen bzw. ihrer rechtm��igen Ersteller. Dieses Projekt soll unter keinen Umst�nden diese bestehenden "Copyrights" anzweifeln oder 
ablehnen. Das Spiel wurde profitfrei entwickelt, und wird kostenfrei vertrieben.

2.Installation:

Mit dem Ausf�hren des Installationsprogramms ist das Spiel unter normalen Umst�nden bereits fertig zum spielen. Es wird keine zus�tzliche Software wie ein Emulator 
o.�. ben�tigt.

Normalerweise wird die Schriftart selbst�ndig installiert. Sollte das nicht der Fall sein und kein Text angezeigt werden, probiert folgendes aus:

Kopiert die beigelegte Schriftart "lunchds" in den Ordner C:\Windows\Fonts zu kopieren. (C: durch den Laufwerksbuchstaben ersetzten, auf dem ihr Windows installiert ist)

3.Standard-Steuerung:

A Schlagen/reden/interagieren
S Springen 
C/Enter reden/interagieren
X schneller laufen
Space oder Z Menue aufrufen/Verlassen
F5-F6 Item ausw�hlen
F7-F8 Skill ausw�hlen
D Zauber/Skill wirken
W Schild
Q Item verwenden
ALT Pause

Die Standard-Einstellung kann in der Truhe ingame ver�ndert werden.

Leider k�nnt ihr wegen der neuen Engine keine alten Savegames der Demo 0.8.1 oder niedriger mehr verwenden (diese w�rden beim laden nur eine Fehlermeldung erzeugen).

4. FAQ:

   F: Warum wird im Spiel keine Schrift angezeigt?
   A: Entweder verwendenst du das Spiel auf einem PC ohne Administartor-Rechte. In diesem Fall kann leider keine Schrift installiert werden. 
      Falls du Administrator-Rechte besitzt, aber dennoch keine Schrift angezeigt bekommst, lies unter Punkt 1. "Installation" nach.

   F: Das Spiel st�rzt ab, wenn ich auf "Laden" gehe!
   A: Stelle sicher, dass im Save-Game-Ordner keinerlei Dateien au�er Savesgames selber sind. Ein Savegame besteht aus einer Datei "Name.rxdata" und einer
      weiteren Datei "~Name.dat"

5.Begriffs-Erkl�rungen:

- KP(Kraftpunkte):
  Die maximale Lebensenergie. Sinken die Kraftpunkte von Ark auf Null, f�llt er in Ohnmacht und startet vom letzten Speicherbuch aus.
  Kann durch Waffen/R�stungen/Elixiere erh�ht und durch Knollen/Tr�nke wiederhergestellt werden.

- ME(Mentale Energie):
  Die maximale Zauberkraft von Ark. Zauber ben�tigen eine bestimmte Anzahl an mentaler Energie, und jeder Zauber entzieht Ark einen Teil
  dieser Energie. Kann durch Waffen/R�stungen/Elixiere erh�ht und durch Knollen/Tr�nke wiederhergestellt werden.

- ATK(Angriff):
  Der Grund-Angriffskraft. Wesentlich effektiver als St�rke. Wird durch Waffen/R�stungen bestimmt.

- STR(Str�rke):
  St�rke erh�ht den Grundschaden, der mit Waffen angerichtet wird. Kann durch Waffen/R�stungen/Elixiere erh�ht werden.

- DEF(Abwehr):
  Der Grund-Wiederstand gegen jegliche Art von Schaden. Kann durch Waffen/R�stungen/Elixiere erh�ht werden.

- AGI(Beweglichkeit):
  Erh�ht die Wahrscheinlichkeit eines kritischen Treffers und l�sst Arks schaden �fter und st�rker nach oben hin variieren. 
  Kann durch Waffen/R�stungen/Elixiere erh�ht werden.

- INT(Intelligenz):
  Bestimmt den Schaden, der durch Zauber angerichtet bzw. geheilt wird. Kann durch Waffen/R�stungen/Elixiere erh�ht werden.

- PDEF(Physische Verteidigung):
  Absorbiert eine gro�en Teil physischen Schadens. Wird bestimmt durch die angelegte R�stung.

- MDEF(Magische Verteidigung):
  Absorbiert einen gro�en Teil magischen Schadens. Wird bestimmt durch die angelegte R�stung.


6.Credits:

Credits gehen u.a. an:
dubealex, f0tz!baerchen, Blizzard(und an alle anderen Authoren die bei TonsofAddons mitgearbeitet haben), 
MGCaladtogel , ToriVerly, ScriptKitty and Dr DJ , Caesar , thunderbolt256, Der Drake, KD, Phantom, Ascare, RAMart, Xiderowg, Moghunter,'Youtube acc MetallSamus; Nick: Metall Ark (XD�)', Neo-Bahamut, H�pfende Kokusnuss,
KiaraXl, Fox!, Flexflexible, Blueshadow, Bredator, Tantrix, Twinstar, Syrus

Eine vollst�ndige Liste ist ingame verf�gbar. 
